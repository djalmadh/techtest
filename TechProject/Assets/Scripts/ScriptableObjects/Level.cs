﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Level", menuName = "Level")]
public class Level : ScriptableObject
{
    public List<string> words, completedWords;
    public int shelfAmount, shelfSpaces, occupiedShelf;
}

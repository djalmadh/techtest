﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;

public class Shelf : MonoBehaviour
{
    public List<GameObject> Letters;
    private ScrollRect scrollRect;
    bool hasCompletedWord;

    private void Start()
    {
        scrollRect = GameObject.FindWithTag("Scroll").GetComponent<ScrollRect>();
        hasCompletedWord = false;
    }

    public void OnBeginDrag(BaseEventData data)
    {
        PointerEventData eventData = (PointerEventData)data;
        ExecuteEvents.Execute(scrollRect.gameObject, eventData, ExecuteEvents.beginDragHandler);
    }

    public void OnDrag(BaseEventData data)
    {
        PointerEventData eventData = (PointerEventData)data;

        ExecuteEvents.Execute(scrollRect.gameObject, eventData, ExecuteEvents.dragHandler);       
    }

    public void OnEndDrag(BaseEventData data)
    {
        PointerEventData eventData = (PointerEventData)data;

        ExecuteEvents.Execute(scrollRect.gameObject, eventData, ExecuteEvents.endDragHandler);
    }

    public void OnDrop(BaseEventData data)
    {
        PointerEventData eventData = (PointerEventData)data;
        var Dletter = eventData.pointerDrag.transform.GetComponent<Letter>();
        if (Dletter != null)
        {
            if (Dletter.islast && Letters.Count < GameController.instance.levelInfo.shelfSpaces && Dletter.draggingSlot)
            {
               // Debug.Log("drop");
                eventData.pointerDrag.transform.SetParent(transform);
                Dletter.parentShelf.Letters.Remove(eventData.pointerDrag.gameObject);

                if (Dletter.parentShelf.hasCompletedWord)
                {                
                    foreach (GameObject a in Dletter.parentShelf.Letters)
                    {
                        a.transform.Find("Text").GetComponent<TextMeshProUGUI>().color = Color.black;
                    }
                    Dletter.transform.Find("Text").GetComponent<TextMeshProUGUI>().color = Color.black;

                    GameController.instance.completedWordsAmount--;
                    Dletter.parentShelf.hasCompletedWord = false;
                }

                Dletter.parentShelf = transform.GetComponent<Shelf>();
                Letters.Add(eventData.pointerDrag.gameObject);
                Dletter.draggingSlot = false;
                Dletter.canvasGroup.blocksRaycasts = true;
                Dletter.oldParent = transform;
                CompleteWord();
            }
        }
       
    }

    public void CompleteWord() 
    {
        string letterString = "";

        for (int i = 0; i < Letters.Count; i++)
        {
            letterString += Letters[i].transform.Find("Text").GetComponent<TextMeshProUGUI>().text;
        }

        if (GameController.instance.levelInfo.completedWords.Contains(letterString))
        {
            //Debug.Log("WordCompleted");
            GameController.instance.completedWordsAmount++;
            hasCompletedWord = true;
            foreach (GameObject a in Letters)
            {
                a.transform.Find("Text").GetComponent<TextMeshProUGUI>().color = Color.green;
            }


            if (GameController.instance.levelInfo.words.Count == GameController.instance.completedWordsAmount)
            {
                GameController.instance.EndGame();
            }
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Letter : MonoBehaviour
{
    private RectTransform rectTransform;
    public CanvasGroup canvasGroup;
    public Vector3 origPosition;
    private Canvas canvas;
    private Transform parent; 
    public Transform  oldParent;

    public Shelf parentShelf;

    private Coroutine co;

    public bool draggingSlot = false;

    public bool islast = false;

    private ScrollRect scrollRect;



    void Start()
    {
        rectTransform = GetComponent<RectTransform>();
        canvasGroup = GetComponent<CanvasGroup>();
        canvas = GameObject.FindWithTag("Canvas").GetComponent<Canvas>();
        parent = transform.parent.parent.parent.parent;
        oldParent = transform.parent;
        scrollRect = GameObject.FindWithTag("Scroll").GetComponent<ScrollRect>();
        parentShelf = transform.parent.GetComponent<Shelf>();
    }

    public void OnPointerDown(BaseEventData data)
    {
        if (co == null) 
        {
            co = StartCoroutine(StartTimer());
        }


        if (parentShelf.Letters.IndexOf(gameObject) == parentShelf.Letters.Count - 1)
        {
            islast = true;
        }
        else
        {
            islast = false;
        }
    }

    public void OnPointerExit(BaseEventData data)
    {
        if (co != null)
        {
            StopCoroutine(co);
            co = null;
        }
    }

    public void OnPointerUp(BaseEventData data)
    {
        if (co != null)
        {
            StopCoroutine(co);
            co = null;
        }
    }

    public void OnPointerEnter(BaseEventData data)
    {
       // Debug.Log("isLast: " + islast + " draggingSlot: "+ draggingSlot);
    }

    private IEnumerator StartTimer()
    {
        yield return new WaitForSeconds(0f);
        if(islast)
            draggingSlot = true;
    }

    public void OnBeginDrag(BaseEventData data)
    {
        PointerEventData eventData = (PointerEventData)data;

        if (draggingSlot)
        {
            if (islast) 
            {
                transform.SetParent(parent);
                origPosition = GetComponent<RectTransform>().anchoredPosition;
                canvasGroup.blocksRaycasts = false;
            }
        }
        else
        {
            ExecuteEvents.Execute(scrollRect.gameObject, eventData, ExecuteEvents.beginDragHandler);
        }         
    }

    public void OnDrag(BaseEventData data)
    {
        PointerEventData eventData = (PointerEventData)data;

        if (draggingSlot)
        {
            if (islast)
            {
                rectTransform.anchoredPosition += eventData.delta / canvas.scaleFactor;
            }
        }
        else
        {
            ExecuteEvents.Execute(scrollRect.gameObject, eventData, ExecuteEvents.dragHandler);
        }
    }

    public void OnEndDrag(BaseEventData eventData)
    {
        ExecuteEvents.Execute(scrollRect.gameObject, eventData, ExecuteEvents.endDragHandler);
        if (draggingSlot)
        {
            if (islast)
            {
                //Debug.Log("ENDDrag");
                canvasGroup.blocksRaycasts = true;
                rectTransform.anchoredPosition = origPosition;
                transform.SetParent(oldParent);
                draggingSlot = false;
                islast = false;
            }
        }
    }
}

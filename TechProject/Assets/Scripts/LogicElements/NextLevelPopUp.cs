﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NextLevelPopUp : MonoBehaviour
{
    public void MainMenu()
    {
        SceneManager.LoadScene("MenuScene");
    }

    public void NextLevel()
    {
        var a = Resources.LoadAll<Level>("Levels").Length;
        var nextLvl = PlayerPrefs.GetInt("CurrentLevel") + 1;

        if (nextLvl <= a) 
        {
            PlayerPrefs.SetInt("CurrentLevel", nextLvl);
            SceneManager.LoadScene("GameScene");
        }
    }
}

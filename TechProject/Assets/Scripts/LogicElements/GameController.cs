﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    public static GameController instance { private set; get;}
    public GameObject letterObj, shelfObj, wordObj, Content, contentPanel, nextLvlPopUp;

    public Level levelInfo;

    public int completedWordsAmount;

    private List<GameObject> shelves;

    void Start()
    {
        if(instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(instance);
            instance = this;
        }

        shelves = new List<GameObject>();
        levelInfo = Resources.Load<Level>("Levels/Level "+ PlayerPrefs.GetInt("CurrentLevel", 1));

        StartGame();
    }

    public void StartGame()
    {
        completedWordsAmount = 0;
        for (int i = 0; i< levelInfo.shelfAmount; i++)
        {
            var obj = Instantiate(shelfObj, Content.transform);
            shelves.Add(obj);
        }

        foreach(string a in levelInfo.completedWords)
        {
            var wordVar = Instantiate(wordObj, contentPanel.transform);
            wordVar.transform.Find("Text").GetComponent<TextMeshProUGUI>().text = a;
        }

        for (int j = 0; j<levelInfo.words.Count; j++) 
        {
            foreach (char a in levelInfo.words[j])
            {
                //var randomNumber = Random.Range(0, levelInfo.occupiedShelf);

                //if (shelves[randomNumber].GetComponent<Shelf>()) 
                //{
                //    while(shelves[randomNumber].GetComponent<Shelf>().Letters.Count >= levelInfo.shelfSpaces)
                //    {
                //        randomNumber = Random.Range(0, levelInfo.occupiedShelf);
                //    }

                    var letterVar = Instantiate(letterObj, shelves[j].transform);
                    var shelfVar = shelves[j].GetComponent<Shelf>();
                    
                    shelfVar.Letters.Add(letterVar);

                    letterVar.transform.Find("Text").GetComponent<TextMeshProUGUI>().text = a.ToString();
               // }
            }

        }
    }
    public void EndGame()
    {
        //Debug.Log("GameEnded");
        StartCoroutine(EndGameRoutine());
    }

    IEnumerator EndGameRoutine()
    {
        yield return new WaitForSeconds(.5f);
        completedWordsAmount = 0;

        foreach (GameObject a in shelves)
        {
            Destroy(a);
        }

        shelves.Clear();

        Instantiate(nextLvlPopUp, GameObject.FindWithTag("Canvas").transform);
    }
}

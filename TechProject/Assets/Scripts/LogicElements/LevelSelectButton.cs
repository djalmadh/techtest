﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelSelectButton : MonoBehaviour
{
    public int LevelNumber;

    public void GoToLevel()
    {
        PlayerPrefs.SetInt("CurrentLevel",LevelNumber);

        SceneManager.LoadScene("GameScene");
    }

    public void ReturnToMain()
    {
        SceneManager.LoadScene("MenuScene");
    }
}
